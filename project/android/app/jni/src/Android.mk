LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := SDLmain

BASE_LIB_PATH := $(LOCAL_PATH)/../../../../../dependent

SRC_PATH := $(LOCAL_PATH)/../../../../../src
SDL_PATH := $(BASE_LIB_PATH)/SDL
SDL_IMAGE_PATH := $(BASE_LIB_PATH)/SDL_image
SDL_MIXER_PATH := $(BASE_LIB_PATH)/lib/SDL_mixer
SDL_TTF_PATH := $(BASE_LIB_PATH)/lib/SDL_ttf

GGE_LIB_PATH := $(LOCAL_PATH)/../../../../../source
LUA_PATH := $(GGE_LIB_PATH)/lua
LOCAL_C_INCLUDES += $(LUA_PATH)

LOCAL_C_INCLUDES := $(SDL_PATH)/include	\
					$(SDL_IMAGE_PATH)	\
					$(SDL_MIXER_PATH)	\
					$(SDL_TTF_PATH)		\
					$(SRC_PATH)			\
					$(LUA_PATH)

# Add your application source files here...
LOCAL_SRC_FILES := 	main.c	\
					init.c

LOCAL_SHARED_LIBRARIES := SDL2			\
						  SDL2_image	\
						  SDL2_mixer	\
						  SDL2_ttf		\
						  lua

LOCAL_LDLIBS := -lGLESv1_CM -lGLESv2 -llog

include $(BUILD_SHARED_LIBRARY)

#$(call import-add-path, $(LOCAL_PATH)/../../../../../dependent)
#$(call import-add-path, $(LOCAL_PATH)/../../../../../source)
#$(call import-module, SDL)
#$(call import-module, SDL_image)
#$(call import-module, SDL_mixer)
#$(call import-module, SDL_ttf)
#$(call import-module, lua)
#$(call import-module, fmod/api/lib/android)
#$(call import-module, HP-Socket/Linux/project/android-ndk/jni)
#$(call import-module, lib/cjson)
#$(call import-module, lib/gastar)
#$(call import-module, lib/ggelua)
#$(call import-module, lib/ghpsocket)
#$(call import-module, lib/gip)
#$(call import-module, lib/gsdl2)
#$(call import-module, lib/gxy)
#$(call import-module, lib/lsqlite3)
#$(call import-module, lib/socket)