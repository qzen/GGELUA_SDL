--[[
    @Author       : GGELUA
    @Date         : 2020-10-11 11:55:41
    @LastEditTime : 2021-05-02 14:47:30
--]]

local ggelua = gge
class  = require("GGE.class")
warn("@on")--开启警告
collectgarbage("generational")--使用分代GC

local type = type
local unpack = table.unpack
local pcall = pcall
local xpcall = xpcall

local function _errfun(err)
    -- local t = {err}
    -- local i = 2
    -- repeat
    --     local r = debug.getinfo(i)
    --     if r and r.what~='C' and r.name~='ggexpcall' then
    --         table.insert(t, string.format('%s:%d: in %s "%s";', r.short_src,r.currentline,r.namewhat,r.name))
    --         local l = 1
    --         repeat
    --             local k,v = debug.getlocal(i,l)
    --             if k and k:sub(1,1)~='(' then
    --                 if type(v)=='string' then
    --                     table.insert(t, string.format('    "local":%s = string: %p(%d);', k,v,#v))
    --                 else
    --                     table.insert(t, string.format('    "local":%s = %s;', k,v))
    --                 end
    --             end
    --             l=l+1
    --         until not k

    --         local u = 1
    --         repeat
    --             local k,v = debug.getupvalue(r.func,u)
    --             if k and k~='_ENV' then
    --                 table.insert(t, string.format('    "upvalue":%s = %s;', k,v))
    --             end
    --             u=u+1
    --         until not k
    --     end
    --     i=i+1
    -- until not r 
    -- err = table.concat(t, "\r" )
    --warn(err)
    
    print(debug.traceback(err))
    --ggelua.messagebox(err)
    if package.loaded.SDL then
        local win = package.loaded.SDL._win
        if win then
            if ggelua.isdebug then
                win:关闭()
            elseif type(win.错误事件)=='function' then
                win.错误事件(win,err)
            end
        end
    end
    return '123'
end

function ggexpcall(fun,...)
    local r = {xpcall(fun,_errfun,...)}
    if r[1]~=nil then
        return unpack(r,2)
    end
    return nil,unpack(r,2)
end

function ggepcall(fun,...)
    local r = {pcall(fun,...)}
    if r[1] then
        return unpack(r,2)
    end
    warn(r[2])
    return nil
end

function ggetype(v)
    local t = type(v)
    if t=='table' or t=='userdata' then
        return v.__name or t
    end
    return t
end

function ggeassert(condition,err,level)
    if condition then
        return condition
    else
        error(err,level+1)
    end
end

if ggelua.entry=='main' then
    print('--------------------------------------------------------------------------------------------------------------------')
    if package.loaded.SDL then
        print(string.format( "%s  %s  %s",_GGEVERSION,_VERSION,require("SDL").GetVersion() ))
    else
        print(string.format( "%s|%s",_GGEVERSION,_VERSION ))
    end
    print("平台:"..ggelua.getplatform().."  调试:"..tostring(ggelua.isdebug).."  控制台:"..tostring(ggelua.isconsole))
    print('--------------------------------------------------------------------------------------------------------------------')
    -- print("引擎目录:"..ggelua.getrunpath())
    -- print("项目目录:"..ggelua.getcurpath())
    -- print('--------------------------------------------------------------------------------------------------------------------')
    -- print('package.path',string.format('"%s"', package.path))
    -- print('package.cpath',string.format('"%s"', package.cpath))
    -- print('--------------------------------------------------------------------------------------------------------------------')    
end