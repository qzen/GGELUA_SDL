--[[
    @Author       : GGELUA
    @Date         : 2021-04-08 01:32:40
    @LastEditTime : 2021-04-13 20:21:00
--]]
local SDL = require("SDL")
local _ENV = setmetatable({}, {__index=_G})
local _pinfo = {
    "使用电池",
    "没有电池",
    "充电中",
    "已充满"
}
function 取电源信息()
    local s,secs,pct = SDL.GetPowerInfo()
    return _pinfo[s],pct
end
if SDL.GetPlatform()=='Android' then
    local _ver = {
        [30] = "11",
        [29] = "10",
        [28] = "9",
        [27] = "8.1",
        [26] = "8.0",
        [25] = "7.1",
        [24] = "7.0",
        [23] = "6.0",
        [22] = "5.1",
        [21] = "5.0",
        [20] = "4.4W",
        [19] = "4.4",
        [18] = "4.3",
        [17] = "4.2",
        [16] = "4.1",
        [15] = "4.0.3",
        [14] = "4.0",
        [13] = "3.2",
        [12] = "3.1",
        [11] = "3.0",
        [10] = "2.3.3",
    }
    function 安卓_版本()
        return _ver[SDl.GetAndroidSDKVersion()]
    end

    function 安卓_是否电视()
        return SDl.IsAndroidTV()
    end

    function 安卓_返回()--触发
        return SDl.AndroidBackButton()
    end

    function 安卓_内存存储路径()--只读
        return SDl.AndroidGetInternalStoragePath()
    end

    function 安卓_外部存储路径()
        if SDL.AndroidGetExternalStorageState()&2==2 then--可写
            return SDl.AndroidGetExternalStoragePath()
        end
    end

    function 安卓_申请权限(t)
        return SDl.AndroidRequestPermission(t)
    end

end

return _ENV