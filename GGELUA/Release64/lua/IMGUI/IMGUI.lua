--[[
    @Author       : GGELUA
    @Date         : 2021-04-24 16:49:40
    @LastEditTime : 2021-05-08 01:47:26
--]]
local SDL = require("SDL")
local im = require"gimgui"
require("IMGUI.按钮")
require("IMGUI.菜单")
im.Init(引擎:取对象())

im._ev = SDL.AddEventHook(function (ev)
    return im.Event(ev)
end)

local IMGUI = class('IMGUI')

function IMGUI:初始化(font)
    self._demo = {false}
end

function IMGUI:开始()
    im.NewFrame()
    if self._demo[1] then
        im.ShowDemoWindow(self._demo)
    end
end

function IMGUI:结束()
    im.EndFrame()
end

function IMGUI:打开DEMO()
    self._demo = {true}
    return self
end

function IMGUI:显示()
    im.Render()
end

return IMGUI