--[[
    @Author       : GGELUA
    @Date         : 2021-04-24 16:25:27
    @LastEditTime : 2021-05-08 01:36:10
--]]
local im = require"gimgui"
local IMBase = require"IMGUI.基类"

local IM输入 = class('IM输入','IMBase')

function IM输入:初始化(name,def,len)
    self._name = name
    self._flag = 0
    local tp = type(def)
    if tp=='string' then
        self._tp = 1
        self[1] = def
        self[2] = len or 128
    elseif tp == 'number' then
        self[1] = def
        if math.floor(def) == def then
            self._tp = 2
        else
            self._tp = 3
        end
    end
end

function IM输入:显示()
    IMBase.更新(self)
    if self._tp == 1 then
        return im.InputText(self._name,self)
    elseif self._tp == 2 then
        return im.InputInt(self._name,self)
    elseif self._tp == 3 then
        return im.InputFloat(self._name,self)
    end
end


return IM输入