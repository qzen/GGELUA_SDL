--[[
    @Author       : GGELUA
    @Date         : 2021-04-24 10:26:45
    @LastEditTime : 2021-04-25 11:29:05
--]]
local im = require"gimgui"
local IMBase = require"IMGUI.基类"

local IM窗口 = class('IM窗口','IMBase')

function IM窗口:初始化(name,def)
    self._name = name
    self._flag = 0
    self[1] = def==true
end

function IM窗口:开始()
    if self[1] then
        if self._x then
            im.SetNextWindowPos(self._x,self._y)
            self._x = nil
            self._y = nil
        end
        if self._w then
            im.SetWindowSize(self._w,self._h)
            self._w = nil
            self._h = nil
        end
        
        return im.Begin(self._name,self)
    end
    return false
end

function IM窗口:结束()
    im.End()
end

function IM窗口:打开()
    self[1] = true
    return self
end

function IM窗口:关闭()
    self[1] = false
    return self
end

function IM窗口:是否打开()
    return self[1]
end

function IM窗口:置标题栏()
    --1 << 0
end

function IM窗口:置调整()
    --1 << 1
end

function IM窗口:置移动()
    --1 << 2
end

function IM窗口:置滑块()
    --1 << 3
end

function IM窗口:置坐标(x,y)
    self._x = x
    self._y = y
    return self
end

function IM窗口:置大小(w,h)
    self._w = w
    self._h = h
    return self
end

function IM窗口:取窗口坐标()
    return im.GetWindowPos()
end

function IM窗口:取窗口大小()
    return im.GetWindowSize()
end

function IM窗口:取窗口宽度()
    return im.GetWindowWidth()
end

function IM窗口:取窗口高度()
    return im.GetWindowHeight()
end

return IM窗口