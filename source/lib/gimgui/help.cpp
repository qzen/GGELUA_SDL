#include "help.h"


bool luaL_optboolean (lua_State *L, int arg,bool def) {
	if (lua_isnoneornil(L, arg)) {
		return def;
	}
	return lua_toboolean(L,arg)?true:false;
}
bool luaL_checkboolean(lua_State* L,int arg){
	luaL_checktype(L,arg,LUA_TBOOLEAN);
	return lua_toboolean(L,arg)?true:false;
}


void getbyref (lua_State *L, int arg,bool &v,int n) {
	luaL_checktype(L,arg,LUA_TTABLE);
	lua_geti(L,arg,n);
	v = lua_toboolean(L,-1)?true:false;
	lua_pop(L,1);
}

void getbyref (lua_State *L, int arg,float &v,int n) {
	luaL_checktype(L,arg,LUA_TTABLE);
	lua_geti(L,arg,n);
	v = (float)luaL_checknumber(L,-1);
	lua_pop(L,1);
}

void getbyref (lua_State *L, int arg,double &v,int n) {
	luaL_checktype(L,arg,LUA_TTABLE);
	lua_geti(L,arg,n);
	v = (double)luaL_checknumber(L,-1);
	lua_pop(L,1);
}

void getbyref (lua_State *L, int arg,int &v,int n) {
	luaL_checktype(L,arg,LUA_TTABLE);
	lua_geti(L,arg,n);
	v = (int)luaL_checkinteger(L,-1);
	lua_pop(L,1);
}

void getbyref (lua_State *L, int arg,unsigned int &v,int n) {
	luaL_checktype(L,arg,LUA_TTABLE);
	lua_geti(L,arg,n);
	v = (int)luaL_checkinteger(L,-1);
	lua_pop(L,1);
}

size_t getbyref (lua_State *L, int arg,const char* &v,int n) {
	luaL_checktype(L,arg,LUA_TTABLE);
	lua_geti(L,arg,n);
	size_t l;
	v = luaL_checklstring(L,-1,&l);
	lua_pop(L,1);
	return l;
}

void setbyref (lua_State *L, int arg,bool v,int n) {
	luaL_checktype(L,arg,LUA_TTABLE);
	lua_pushboolean(L,v);
	lua_seti(L,arg,n);
}

void setbyref (lua_State *L, int arg,float v,int n) {
	luaL_checktype(L,arg,LUA_TTABLE);
	lua_pushnumber(L,v);
	lua_seti(L,arg,n);
}

void setbyref (lua_State *L, int arg,double v,int n) {
	luaL_checktype(L,arg,LUA_TTABLE);
	lua_pushnumber(L,v);
	lua_seti(L,arg,n);
}

void setbyref (lua_State *L, int arg,int v,int n) {
	luaL_checktype(L,arg,LUA_TTABLE);
	lua_pushinteger(L,v);
	lua_seti(L,arg,n);
}

void setbyref (lua_State *L, int arg,unsigned int v,int n) {
	luaL_checktype(L,arg,LUA_TTABLE);
	lua_pushinteger(L,v);
	lua_seti(L,arg,n);
}

void setbyref (lua_State *L, int arg,const char* v,int n) {
	luaL_checktype(L,arg,LUA_TTABLE);
	lua_pushstring(L,v);
	lua_seti(L,arg,n);
}

void getbyreftable (lua_State *L, int arg,float v[]) {
	luaL_checktype(L,arg,LUA_TTABLE);
	int n = (int)(sizeof(v)/sizeof(v[0]));
	for (int i=0;i<n;i++)
	{
		lua_geti(L,arg,i+1);
		v[i] = (float)luaL_checknumber(L,-1);
	}
	lua_pop(L,n);
}

void getbyreftable (lua_State *L, int arg,int v[]) {
	luaL_checktype(L,arg,LUA_TTABLE);
	int n = (int)(sizeof(v)/sizeof(v[0]));
	for (int i=0;i<n;i++)
	{
		lua_geti(L,arg,i+1);
		v[i] = (int)luaL_checkinteger(L,-1);
	}
	lua_pop(L,n);
}

void getbyreftable (lua_State *L, int arg,const char* v[]) {
	luaL_checktype(L,arg,LUA_TTABLE);
	int n = (int)(sizeof(v)/sizeof(v[0]));
	for (int i=0;i<n;i++)
	{
		lua_geti(L,arg,i+1);
		v[i] = luaL_checkstring(L,-1);
	}
	lua_pop(L,n);
}

void setbyreftable (lua_State *L, int arg,float v[]) {
	luaL_checktype(L,arg,LUA_TTABLE);
	int n = (int)(sizeof(v)/sizeof(v[0]));
	for (int i=0;i<n;i++)
	{
		lua_pushnumber(L,v[i]);
		lua_seti(L,arg,i+1);
	}
}

void setbyreftable (lua_State *L, int arg,int v[]) {
	luaL_checktype(L,arg,LUA_TTABLE);
	int n = (int)(sizeof(v)/sizeof(v[0]));
	for (int i=0;i<n;i++)
	{
		lua_pushinteger(L,v[i]);
		lua_seti(L,arg,i+1);
	}
}

void setbyreftable (lua_State *L, int arg,char* v[]) {
	luaL_checktype(L,arg,LUA_TTABLE);
	int n = (int)(sizeof(v)/sizeof(v[0]));
	for (int i=0;i<n;i++)
	{
		lua_pushstring(L,v[i]);
		lua_seti(L,arg,i+1);
	}
}

int gettableVec2 (lua_State *L, int arg,ImVec2 &v) {
	if (lua_istable(L,arg))
	{
		lua_geti(L,arg,1);
		v.x = (float)luaL_checknumber(L,-1);
		lua_geti(L,arg,2);
		v.y = (float)luaL_checknumber(L,-1);
		lua_pop(L,2);
		return 1;
	}
	return 0;
}

void settableVec2 (lua_State *L, int arg,ImVec2 v) {
	if (lua_istable(L,arg))
	{
		lua_pushnumber(L,v.x);
		lua_seti(L,arg,1);
		lua_pushnumber(L,v.y);
		lua_seti(L,arg,2);
	}
}

void gettableVec4 (lua_State *L, int arg,ImVec4 &v) {
	if (lua_istable(L,arg))
	{
		lua_geti(L,arg,1);
		v.x = (float)luaL_checknumber(L,-1);
		lua_geti(L,arg,2);
		v.y = (float)luaL_checknumber(L,-1);
		lua_geti(L,arg,3);
		v.z = (float)luaL_checknumber(L,-1);
		lua_geti(L,arg,4);
		v.w = (float)luaL_checknumber(L,-1);
		lua_pop(L,4);
	}
}
void settableVec4 (lua_State *L, int arg,ImVec4 v) {
	if (lua_istable(L,arg))
	{
		lua_pushnumber(L,v.x);
		lua_seti(L,arg,1);
		lua_pushnumber(L,v.y);
		lua_seti(L,arg,2);
		lua_pushnumber(L,v.z);
		lua_seti(L,arg,3);
		lua_pushnumber(L,v.w);
		lua_seti(L,arg,4);
	}
}

int getImVec2(lua_State* L, int n, ImVec2& v)
{
	v.x = (float)luaL_optnumber(L,n,v.x);
	v.y = (float)luaL_optnumber(L,n+1,v.y);
	return !(lua_isnoneornil(L, n)&&lua_isnoneornil(L, n+1));
}

int getImVec2(lua_State* L, int *n, ImVec2& v)
{
	int r = !(lua_isnoneornil(L, *n) && lua_isnoneornil(L, *n + 1));
	v.x = (float)luaL_optnumber(L, (*n)++, v.x);
	v.y = (float)luaL_optnumber(L, (*n)++, v.y);
	return r;
}

void pushImVec2(lua_State* L,ImVec2 v){
	lua_pushnumber(L,v.x);
	lua_pushnumber(L,v.y);
}

void pushImVec4(lua_State* L,ImVec4 v){
	lua_pushnumber(L,v.x);
	lua_pushnumber(L,v.y);
	lua_pushnumber(L,v.z);
	lua_pushnumber(L,v.w);
}

int getImVec4(lua_State* L, int n, ImVec4& v)
{
	v.x = (float)luaL_optnumber(L,n++,v.x);
	v.y = (float)luaL_optnumber(L,n++,v.y);
	v.z = (float)luaL_optnumber(L,n++,v.z);
	v.w = (float)luaL_optnumber(L,n++,v.w);
	return 0;
}

int getImVec4(lua_State* L, int* n, ImVec4& v)
{
	v.x = (float)luaL_optnumber(L, (*n)++, v.x);
	v.y = (float)luaL_optnumber(L, (*n)++, v.y);
	v.z = (float)luaL_optnumber(L, (*n)++, v.z);
	v.w = (float)luaL_optnumber(L, (*n)++, v.w);
	return 0;
}

void getinputbuf(lua_State *L,int arg,char*&buf,size_t &buf_size){
	int top = lua_gettop(L);
	luaL_checktype(L,arg,LUA_TTABLE);
	lua_rawgeti(L,arg,2);//{'����',����,[0]=buf}
	buf_size = luaL_checkinteger(L,-1)+1;
	if (lua_rawgeti(L,arg,0)==LUA_TUSERDATA){
		buf = (char*)lua_touserdata(L,-1);
	}else{
		const char* s ;
		size_t len;
		lua_rawgeti(L,arg,1);//���� 
		s = luaL_checklstring(L,-1,&len);
		
		if(len>=buf_size)
			luaL_error(L,"len error!");
		buf = (char*)lua_newuserdata(L,buf_size);
		memset(buf,0,buf_size);
		memcpy(buf,s,len);
		lua_rawseti(L,arg,0);
	}
	lua_settop(L,top);
}

void setinputbuf(lua_State *L,int arg,char* buf){
	lua_pushstring(L,buf);
	lua_rawseti(L,arg,1);
}

//PlotLines
float values_getter(void* data, int idx){
	tabledata* t = (tabledata*)data;
	lua_geti(t->L,t->arg,idx+1);
	float n  = (float)luaL_checknumber(t->L,-1);
	lua_pop(t->L,1);
	return n;
}
bool items_getter(void* data, int idx, const char** out_text){
	tabledata* t = (tabledata*)data;
	lua_geti(t->L,t->arg,idx+1);
	*out_text = lua_tostring(t->L,-1);
	lua_pop(t->L,1);
	return true;
}
int input_callback(ImGuiInputTextCallbackData *data){

	return 0;
}