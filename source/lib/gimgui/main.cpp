#include "help.h"
#include <SDL.h>
#include "backends/imgui_impl_sdl.h"
#include "backends/imgui_impl_sdlrenderer.h"

//static SDL_Renderer * g_Renderer = NULL;
static SDL_Window* g_window = NULL;

static int Event(lua_State* L)
{
	SDL_Event * ev = (SDL_Event*)luaL_checkudata(L, 1, "SDL_Event");
	//lua_pushboolean(L,ImGui_ImplSDL2_ProcessEvent(ev));
	if (ImGui_ImplSDL2_ProcessEvent(ev))
	{
		ImGuiIO& io = ImGui::GetIO();
		lua_pushboolean(L, io.WantCaptureMouse || io.WantCaptureKeyboard);
		return 1;
	}
	return 0;
}

static int Render(lua_State* L)
{
	ImGui::Render();
	ImGui_ImplSDLRenderer_RenderDrawData(ImGui::GetDrawData());
	return 0;
}

static int GetIO(lua_State* L)
{
	ImGuiIO& io = ImGui::GetIO();
	lua_pushboolean(L,io.WantCaptureMouse);
	return 1;
}

static int GetStyle(lua_State* L)
{

	return 0;
}

static int NewFrame(lua_State* L)
{
	//ImGui_ImplSDLRenderer_NewFrame();
	ImGui_ImplSDL2_NewFrame(g_window);
	ImGui::NewFrame();
	return 0;
}

static int EndFrame(lua_State* L)
{
	ImGui::EndFrame();
	return 0;
}

static int ShowDemoWindow(lua_State* L)
{
	bool v;
	getbyref(L,1,v);
	ImGui::ShowDemoWindow(&v);
	setbyref(L,1,v);
	return 0;
}

static int ShowAboutWindow(lua_State* L)
{
	bool v;
	getbyref(L,1,v);
	ImGui::ShowAboutWindow(&v);
	setbyref(L,1,v);
	return 0;
}

static int ShowMetricsWindow(lua_State* L)
{
	bool v;
	getbyref(L,1,v);
	ImGui::ShowMetricsWindow(&v);
	setbyref(L,1,v);
	return 0;
}

static int ShowStyleEditor(lua_State* L)
{
	ImGui::ShowStyleEditor();
	return 0;
}

static int ShowStyleSelector(lua_State* L)
{
	bool r = ImGui::ShowStyleSelector(luaL_checkstring(L,1));
	lua_pushboolean(L,r);
	return 1;
}

static int ShowFontSelector(lua_State* L)
{
	ImGui::ShowFontSelector(luaL_checkstring(L,1));
	return 0;
}

static int ShowUserGuide(lua_State* L)
{
	ImGui::ShowUserGuide();
	return 0;
}

static int GetVersion(lua_State* L)
{
	lua_pushstring(L,ImGui::GetVersion());
	return 1;
}

static int StyleColorsDark(lua_State* L)
{
	ImGui::StyleColorsDark();
	return 0;
}

static int StyleColorsClassic(lua_State* L)
{
	ImGui::StyleColorsClassic();
	return 0;
}

static int StyleColorsLight(lua_State* L)
{
	ImGui::StyleColorsLight();
	return 0;
}

static int CreateContext(lua_State* L)
{
	const char* ttf = "c:\\Windows\\Fonts\\simsun.ttc";// luaL_checkstring(L, 1);
	size_t size = 16;// luaL_optinteger(L, 2, 16);
	ImGuiContext* im = ImGui::CreateContext();
	ImGuiIO& io = ImGui::GetIO();
	io.Fonts->AddFontFromFileTTF(ttf, (float)size, NULL, io.Fonts->GetGlyphRangesChineseFull());
	ImGui_ImplSDL2_InitForSDLRenderer(g_window);
	ImGui_ImplSDLRenderer_Init(g_window);
	ImGui_ImplSDLRenderer_CreateFontsTexture();//ImGui_ImplSDLRenderer_NewFrame

	return 0;
}

static int DestroyContext(lua_State* L)
{
	return 0;
}

static int SetCurrentContext(lua_State* L)
{
	return 0;
}

static int Init(lua_State* L)
{
	SDL_Window * window = *(SDL_Window**)luaL_checkudata(L, 1, "SDL_Window");
	g_window = window;
	CreateContext(L);
	return 0;
}

static int Shutdown(lua_State* L) 
{
	//ImGui_ImplSDLRenderer_Shutdown();
	ImGui_ImplSDL2_Shutdown();
	ImGui::DestroyContext();
	return 0;
}

extern "C" LUALIB_API
	int luaopen_gimgui(lua_State* L)
{
	luaL_Reg funcs[] = {
		{"Init",Init},
		{"__gc",Shutdown},
		{"Event",Event},
		{"NewFrame",NewFrame},
		{"EndFrame",EndFrame},
		{"Render", Render},
		//CreateContext
		//DestroyContext
		//GetCurrentContext
		//SetCurrentContext
		{"GetIO",GetIO},
		{"GetStyle",GetStyle},

		{"ShowDemoWindow",ShowDemoWindow},
		{"ShowMetricsWindow",ShowMetricsWindow},
		{"ShowAboutWindow",ShowAboutWindow},
		{"ShowStyleEditor",ShowStyleEditor},
		{"ShowStyleSelector",ShowStyleSelector},
		{"ShowFontSelector",ShowFontSelector},
		{"ShowUserGuide",ShowUserGuide},
		{"GetVersion",GetVersion},

		{"StyleColorsDark",StyleColorsDark},
		{"StyleColorsClassic",StyleColorsClassic},
		{"StyleColorsLight",StyleColorsLight},
		
		{NULL, NULL},
	};

	luaL_newlib(L, funcs);
	lua_pushvalue(L, -1);
	lua_setmetatable(L, -2);
	bind_widgets(L);
	return 1;
}