#pragma once

#include "lua.hpp"
#include "imgui.h"
#include "SDL.h"

#define luaL_checkint(L,n)	((int)luaL_checkinteger(L, (n)))
#define luaL_optint(L,n,d)	((int)luaL_optinteger(L, (n), (d)))
#define luaL_checkfloat(L,n)	((float)luaL_checknumber(L, (n)))
#define luaL_optfloat(L,n,d)	((float)luaL_optnumber(L, (n), (d)))

struct tabledata {
	lua_State *L;
	int arg;
	tabledata(lua_State *_L,int _arg)  { L = _L ,arg= _arg; }
};

bool luaL_optboolean (lua_State *L, int arg,bool def);
bool luaL_checkboolean(lua_State* L,int arg);

void getbyref (lua_State *L, int arg,bool &v,int n=1);
void getbyref (lua_State *L, int arg,float &v,int n=1);
void getbyref (lua_State *L, int arg,double &v,int n=1);
void getbyref (lua_State *L, int arg,int &v,int n=1);
void getbyref (lua_State *L, int arg,unsigned int &v,int n=1);
size_t getbyref (lua_State *L, int arg,const char* &v,int n=1);

void setbyref (lua_State *L, int arg,bool v,int n=1);
void setbyref (lua_State *L, int arg,float v,int n=1);
void setbyref (lua_State *L, int arg,double v,int n=1);
void setbyref (lua_State *L, int arg,int v,int n=1);
void setbyref (lua_State *L, int arg,unsigned int v,int n=1);
void setbyref (lua_State *L, int arg,const char* v,int n=1);

void getbyreftable (lua_State *L, int arg,float v[]);
void getbyreftable (lua_State *L, int arg,int v[]);
void getbyreftable (lua_State *L, int arg,const char* v[]);

void setbyreftable (lua_State *L, int arg,float v[]);
void setbyreftable (lua_State *L, int arg,int v[]);
void setbyreftable (lua_State *L, int arg,char* v[]);

int gettableVec2 (lua_State *L, int arg,ImVec2 &v);
void settableVec2 (lua_State *L, int arg,ImVec2 v);

void gettableVec4 (lua_State *L, int arg,ImVec4 &v);
void settableVec4 (lua_State *L, int arg,ImVec4 v);

int getImVec2(lua_State* L, int n, ImVec2& v);
int getImVec2(lua_State* L, int *n, ImVec2& v);

void pushImVec2(lua_State* L,ImVec2 v);

int getImVec4(lua_State* L, int arg, ImVec4& v);
int getImVec4(lua_State* L,int *arg,ImVec4 &v);

void pushImVec4(lua_State* L,ImVec4 v);

void getinputbuf(lua_State *L,int arg,char*&buf,size_t &buf_size);
void setinputbuf(lua_State *L,int arg,char* buf);


float values_getter(void* data, int idx);
bool items_getter(void* data, int idx, const char** out_text);

int bind_widgets(lua_State* L);